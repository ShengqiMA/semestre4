#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <iostream>
using namespace std;

int main(){
  IMG_Init(IMG_INIT_PNG);
  if(SDL_Init(SDL_INIT_VIDEO) < 0){
    cout << "could not initalize!" << endl;
  }
  else{
    SDL_Window * window = SDL_CreateWindow("Study",100,100,1000,1000,SDL_WINDOW_SHOWN);
    if(window == NULL){
      cout << "window could not be created!"<< endl;
    }
    else{
      SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
      if(renderer == NULL){
        cout << "load renderer false" << endl;
      }
      SDL_Surface * pic = IMG_Load("sdl_test.png");
      if(pic == NULL){
        cout << "load picture false: " << SDL_GetError() << endl;
      }
      SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, pic);
      if(texture == NULL) cout << "create texture false" << endl;
      SDL_RenderCopy(renderer, texture, NULL, NULL);
      SDL_RenderPresent(renderer);

      bool quit = false;
      SDL_Event event;

      while(!quit){
        SDL_WaitEvent(&event);
        switch (event.type) {
          case SDL_QUIT:
            quit = true;
            break;
        }
      }
      SDL_DestroyTexture(texture);
      SDL_FreeSurface(pic);
      SDL_DestroyRenderer(renderer);
      SDL_DestroyWindow(window);
    }
  }
  return 0;
}
