#include "Image.h"
#include <cassert>
#include <fstream>
#include <iostream>

using namespace std;


    Image::Image(){
        dimx=0;
        dimy=0;
    }
    
    Image::Image(unsigned int dimensionX,unsigned int dimensionY){
        if(dimensionX >=0&&dimensionY>=0){
            dimx=dimensionX; 
            dimy=dimensionY;
            tab = new Pixel(0,0,0);
        }
    }

    Image::~Image(){
        dimy=0;
        dimx=0;
        delete [] tab;
        tab = NULL;
    }

    Pixel & Image::getPix(const int x,const int & y)const{
        if(x>=0 &&y>=0){
            return tab[y*dimx+x];
        }
    }

    void Image::setPix(unsigned int x,unsigned int y,const Pixel & couleur){
        if(x>=0 && y>=0){
            tab[y*dimx+x]=couleur;
        }else{return;}
    }

    void Image::dessinerRectangle(unsigned int Xmin,unsigned int Ymin,unsigned int Xmax,unsigned int Ymax,const Pixel & couleur){
        for(unsigned int x=Xmin;x<Xmax;x++){
            for(unsigned int y=Ymin;y<Ymax;y++){
                setPix(x,y,couleur);
            }
        }
    }

    void Image::effacer(const Pixel & couleur){
        dessinerRectangle(0,0,dimx,dimy,couleur);
    }


void Image::sauver(const string & filename) const {
    ofstream fichier (filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel& pix = getPix(x++,y);
            fichier << + pix.getRouge() << " " << + pix.getVert() << " " << + pix.getBleu() << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string & filename) {
    ifstream fichier (filename.c_str());
    assert(fichier.is_open());
	unsigned char r,g,b;
	string mot;
	dimx = dimy = 0;
	fichier >> mot >> dimx >> dimy >> mot;
	assert(dimx > 0 && dimy > 0);
	if (tab != NULL) delete [] tab;
	tab = new Pixel [dimx*dimy];
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            fichier >> r >> b >> g;
            getPix(x,y).setRouge(r);
            getPix(x,y).setVert(g);
            getPix(x,y).setBleu(b);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole(){
    cout << dimx << " " << dimy << endl;
    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel & pix = getPix(x,y);
            cout << + pix.getRouge() << " " << + pix.getVert() << " " << +pix.getBleu() << " ";
        }
        cout << endl;
    }
}

    void Image::testRegression(){
        Image * im = new Image();
        Image * im2 = new Image(10,10);

        delete im;

        Pixel bleu(0,0,255);
        im2->setPix(3,3,bleu);
        Pixel t = im2->getPix(3,3);
        unsigned char r,g,b;

        r = t.getRouge();
        g = t.getVert();
        b = t.getBleu();

        cout<< "Le pixel test a cette couleur r: "<<int(r) <<" g: "<<int(g) <<" b: "<<int(b) <<endl; 
        Pixel vert(0,255,0);
         im2->dessinerRectangle(0,0,10,10,vert);
         im2->afficherConsole();
    }

