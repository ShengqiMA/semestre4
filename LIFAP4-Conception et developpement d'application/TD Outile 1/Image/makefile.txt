all: test.out

test.out: mainTest.o Image.o Pixel.o
	g++ -g mainTest.o Pixel.o Image.o -o test.out

mainTest.o: mainTest.cpp Pixel.h Image.h
	g++ -g -Wall -c mainTest.cpp

Image.o: Image.h Image.cpp Pixel.h
	g++ -g -Wall -c Image.cpp

Pixel.o: Pixel.h Pixel.cpp 
	g++ -g -Wall -c Pixel.cpp

clean:
	rm *.o *.out

veryclean: clean
	rm *.out
