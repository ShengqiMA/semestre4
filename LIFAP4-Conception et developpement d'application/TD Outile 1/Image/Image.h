#ifndef _IMAGE_TD
#define _IMAGE_TD

#include "Pixel.h"
#include <iostream>
#include <string>
#include <string.h>

#include <cassert>
#include <fstream>


using namespace std;

class Image{
    private:
        Pixel * tab;
        unsigned int dimx,dimy;

        public:

        Image();
        Image(unsigned int dimensionX,unsigned int dimensionY);
        ~Image();
        Pixel & getPix(const int x,const int & y)const;
        void setPix(unsigned int x,unsigned int y,const Pixel & couleur);
        void dessinerRectangle(unsigned int Xmin,unsigned int Ymin,unsigned int Xmax,unsigned int Ymax,const Pixel & couleur);
        void effacer(const Pixel & couleur);
        void sauver(const string & filename) const;
        void ouvrir(const string & filename);
        void afficherConsole();
        void testRegression();


};



#endif