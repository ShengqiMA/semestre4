#ifndef _PIXEL_TD
#define _PIXEL_TD

class Pixel{
    private:
    unsigned char r,g,b;
    public:
    Pixel();
    Pixel(unsigned char nr,unsigned char ng,unsigned char nb);
    unsigned char getRouge()const;
    unsigned char getVert()const;
    unsigned char getBleu()const;
    void setRouge(unsigned char nr);
    void setVert(unsigned char ng);
    void setBleu(unsigned char nb);

};



#endif