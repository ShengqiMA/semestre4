/*MA Shengqi*/
/*P2108131*/

/*exo1*/
/*compresse(L1,L2) L1:liste donnée L2:liste résultat*/
compresse([],[]).
compresse([X|L1],[X|L3]):-enleve(L1,X,L2),compresse(L2,L3).
/*enleve(L1,X,L2) L1:liste donnée X:événement à enleve L2:liste résultat*/
enleve([],_,[]).
enleve([X|L],X,L):-!.
enleve([Y|L],X,[Y|L1]):-enleve(L,X,L1).

/*
?- compresse([1,2,2,1,3,3,4,4,2,3],X).
X = [1, 2, 3, 4]
 */


/*exo2*/
/*remplace_un(L1,L,L2) L1:liste donnée, L:liste de remplacement, L2:liste résultat*/
remplace(L1,[X],R):-remplace_un(L1,X,R).
remplace(L1,[X|L],R):-remplace_un(L1,X,L2),remplace(L2,L,R).
/*
?- remplace([a,a,e,b,b,b,c,c,a,a,d,e,e,e],[[a,z],[e,g]],X).
X = [z, z, g, b, b, b, c, c, z|...]
?- remplace_un([a,a,b,b,b,c,c,a,a,d,e,e,e],[a,z],X).
X = [z, z, b, b, b, c, c, z, z|...]
 */

/*remplace_un(L1,[X,Y],L2) L1:liste donnée, remplacer tous les X avec Y, L2:liste résultat*/
remplace_un([],_,[]).
remplace_un([X|L],[X,Y],[Y|L1]):-remplace_un(L,[X,Y],L1).
remplace_un([Z|L],[X,Y],[Z|L1]):- Z\==X,remplace_un(L,[X,Y],L1).

/*
correction:
remplace([], _, [])
remplace([X|L], Lr, [Y|R]) :- member([X, Y], Lr), !, remplace(L, Lr, R).
remplace([X|K], Lr, [X|R]) :- remplace(L, Lr, R).

 */
/*exo3*/
bassesse([],0).
bassesse([N,[],[]],1):-N\==[].
bassesse([N,G,[]],1):-N\==[],G\==[].
bassesse([N,[],D],1):-N\==[],D\==[].
bassesse([N,G,D],R):-N\==[],D\==[],G\==[],bassesse(G,NG),bassesse(D,ND),NG =< ND,R is NG + 1.
bassesse([N,G,D],R):-N\==[],D\==[],G\==[],bassesse(G,NG),bassesse(D,ND),NG > ND,R is ND + 1.
/*
?- bassesse([5,[4,[2,[],[]],[3,[],[]]],[6,[7,[],[]],[2,[],[]]]],N).
N = 3 .
?- bassesse([5,[4,[],[]],[6,[],[]]],N).
N = 2 .
?- bassesse([5,[4,[],[]],[]],N).
N = 1 .
*/

/*
correction
bassesse([], 0).
bassesse([_,G,D], B):-bassesse(G, BG), bassesse(D, BD), B is min(BG, BD)+1
 */

/*exo4*/
/*contraine*/
somme_ligne([A,B,C,D,E,F,G,H,I]):-
A+B+C =:= D+E+F,
A+B+C =:= G+H+I,
A+B+C =:= A+D+G,
A+B+C =:= B+E+H,
A+B+C =:= C+F+I,
A+B+C =:= A+E+I,
A+B+C =:= C+E+G.
/*correction
S is A + B + C
S =:= D + E + F
...
 */

/*trouver tous les solutions*/
allowed([],[]).
allowed(L,[X|L2]) :- member(X,L), enleve(L,X,L1), allowed(L1,L2).

magique(L1,L2):-allowed(L1,L2),somme_ligne(L2).
/*
Il y a 8 solutions dont tous les cas de L1

?- findall(L,magique([1,2,3,4,5,6,7,8,9],L),L1), length(L1,N).
L1 = [[2, 7, 6, 9, 5, 1, 4, 3|...], [2, 9, 4, 7, 5, 3, 6|...], [4, 3, 8, 9, 5, 1|...], [4, 9, 2, 3, 5|...], [6, 1, 8, 7|...], [6, 7, 2|...], [8, 1|...], [8|...]],
N = 8.
*/


/*exo5*/
/*recherche(EtatCourant,EtatFinal,ListeTaboue,ListeOp) tous donnes sauf ListeOp */
/*Eg = Etat gauche*/
/*Ed = Etat droite*/
/*Esg = Etat suivent gauche*/
/*Esd = Etat suivent droite*/
/*Opg = operateur gauche*/
/*Opd = operateur droite*/
/*
recherche(Ef,Ef,_,[]) :- !.
recherche(Eg,Ed,Le,[Opg,Opd|Lop]) :-
              transferer_gauche(Eg,Esg).
              operateur(Eg,Opg,Esg),
              operateur(Ed,Opd,Esd),
							not(member(Esg,Le)),
							not(member(Esd,Le)),
							write(Opg),nl,write(Opd),nl,
              write('__________'),nl,
							recherche(Esg,Ef,Le,Lop).
transferer_gauche([X,Y,Z],[X,Z]).
transferer_gauche([X,Y],[X]).

resoudre(Sol) :- etatInitial(Ei),etatFinal(Ef),recherche(Ei,Ef,[[mais,canard],[canard,renard],[renard,canard],[canard,mais]],Sol).

etatInitial([mais,canard,renard]).
etatFinal([mais,canard,renard]).
etatFinal([mais,renard,canard]).
etatFinal([canard,mais,renard]).
etatFinal([canard,renard,mais]).
etatFinal([renard,mais,canard]).
etatFinal([renard,canard,mais]).

operateur(E1,[E1,E2],E2).
*/

/*correction
probleme de recherche
recherche(Ec, Ef, Letats, [Op|Lcp]):-
operateur(Ec, Op, Es),
not(member(Es, Letats)),
not(interdit(Es)),
recherche(Es, Ef, [Es|Letats], Lcp).

resoudre(Sol):-initial(Ei), final(Ef), recherche(Ei, Ef, [Ei], Sol).

correction exo5:

initial([g,g,g,g]).
final([d,d,d,d]).

change(g,d).
change(d,g).

operateur([X,R,X,M], [NX, R, X, M]):-change(X, NX).

interdit([PositionHomme, PositionRenard, PositionCanard, PositionMais])
interdit([Y, X, X, _]):-Y \== X.
interdit([Y, _, X, X]):-Y \== X.
*/
