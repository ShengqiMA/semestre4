/*1.Labyrinthe*/
couloir(entree, thesee).
couloir(entree, ariane).
couloir(thesee, minotaure).
couloir(thesee,sombre).
couloir(claire, sombre).
couloir(claire, sortie).
couloir(minotaure, sortie).
couloir(ariane, claire).
couloir(sombre,sortie).

passage(X,Z) :- couloir(X,Z).
passage(X,Z) :- couloir(Z,X).

chemin(X,X,[X],_):-!.
chemin(X,Y,[X|L],Ltaboue):-passage(X,Z),not(member(Z,Ltaboue)),chemin(Z,Y,L,[Z|Ltaboue]).
/* Pour avoir une solution :
ָ��������·��������·
?- chemin(entree,sortie,Parcours,[entree]),member(ariane,Parcours),not(member(minotaure,Parcours)).
Parcours = [entree, ariane, claire, sombre, sortie] ;
Parcours = [entree, ariane, claire, sortie] ;
No
*/

/*2.Coupure*/
soustrait([],_,[]).
soustrait([X|L],L2,L3) :- member(X,L2), soustrait(L,L2,L3).
soustrait([X|L1],L2,[X|L3]):- not(member(X,L2)),soustrait(L1,L2,L3).

/*3.N��gation*/
disjoints(L1,L2) :- not((member(X,L1),member(X,L2))).
/*G��n��ration*/
/* permutation */
permute([],[]).
permute(L,[X|L2]) :- member(X,L), enleve(X,L,L1), permute(L1,L2).

enleve(_,[],[]).
enleve(X,[X|L],L) :- !.
enleve(X,[Y|L],[Y|L1]) :- enleve(X,L,L1).

/*Triangle de nombres*/
/*L���г��� length(L,Long)*/
somme_cote([N1,N2,N3,N4,N5,N6]):-(N1+N2+N3)=:=(N3+N4+N5),(N3+N4+N5)=:=(N1+N6+N5).
triangle(L1,L2):-permute(L1,L2),somme_cote(L2).
findall(L,triangle([1,2,3,4,5,6],L),L1), length(L1,N).

/*6.Pour aller plus loin*/
/* premiere version : repond oui a aime(toto,jardinage) parce que n'unifie pas aime(toto,jardinage)
avec la tete de clause aime(X,sport) et passe donc directement a la suivante */
jeune(alfred).
jeune(toto).
aime(X,sport) :- jeune(X),!.
aime(_,jardinage).

/* version corrigee */
aime2(X,Quoi) :- jeune(X),!,Quoi=sport.
aime2(_,jardinage).

/* inserer(X,L1,L2) l'insertion de X dans la liste trie L1 donne L2 */
 inserer(X,[],[X]).
 inserer(X,[Y|L],[X,Y|L]) :- X=<Y.
 inserer(X,[Y|L],[Y|L1]) :- X>Y, inserer(X,L,L1).

 /* version en remplaant le test par une coupure */
 inserer2(X,[],[X]).
 inserer2(X,[Y|L],[X,Y|L]) :- X=<Y,!.
 inserer2(X,[Y|L],[Y|L1]) :- inserer2(X,L,L1).

 /* meme probleme que precedemment pour le test faux : n'unifie pas avec la tete de clause
 donc ne passe pas sur la coupure. Moralite : comme d'habitude, quand on ecrit le predicat en
 pensant a une utilisation (ici calculer le troisieme argument) il est dangereux de l'utiliser
 autrement */

 /* version qui marche 	aussi dans le cas du test faux */
 inserer3(X,[],[X]).
 inserer3(X,[Y|L],T) :- X=<Y,!,T=[X,Y|L].
 inserer3(X,[Y|L],[Y|L1]) :- inserer3(X,L,L1).
