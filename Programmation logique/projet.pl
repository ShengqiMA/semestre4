/*
NOM: MA
PRENOM: Shengqi
NOETU: p2108131
*/

/*1. Représentation du problème de planification d'examen*/
/*inscrits(UE, List_nom)*/
inscrits(lif1, [marc, jean, paul, pierre, anne, sophie, thierry, jacques]).
inscrits(lif2, [anne, pierre, emilie, antoine, juliette]).
inscrits(lif3, [juliette, antoine, thierry, jean, edouard]).
inscrits(lif4, [andre, olivier, beatrice]).
inscrits(lif5, [edouard, paul, pierre, emilie]).
inscrits(lif6, [andre, beatrice, amelie]).

/*variable et valeurs*/
variable([lif1,lif2, lif3, lif4, lif5, lif6]).
valeurs([1,2,3,4]).


/*domaines(L)*/

/*on construit une couple avec le premier élément d'une liste donnée et valeurs*/
couple([X|_],[X,V]):-valeurs(V).
/*on construit une liste de couple avec une liste donnée */
list_couple([],[]).
list_couple([X|L],[R|R1]):-couple([X|L],R),list_couple(L,R1).
/*on construit une liste domaines avec une liste de variable*/
domaines(L):-list_couple(VAR,L),variable(VAR).

/*résultats des tests
?- domaines(L).
[[lif1,[1,2,3,4,5,6,7,8,9,10]],[lif2,[1,2,3,4,5,6,7,8,9,10]],[lif3,[1,2,3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[1,2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
L = [[lif1, [1, 2, 3, 4, 5, 6|...]], [lif2, [1, 2, 3, 4, 5|...]], [lif3, [1, 2, 3, 4|...]], [lif4, [1, 2, 3|...]], [lif5, [1, 2|...]], [lif6, [1|...]]]
*/
/*consistants([X1,V1],[X2,V2])*/
/*vrai si la variable X1 peut prendre la valeur V1 en meme temp que X2 peut prendre la valeur V2*/
consistants([X1,V1],[X2,V2]):-variable(VAR), valeurs(VAL),
member(X1,VAR),
member(X2,VAR),
member(V1,VAL),
member(V2,VAL),
V1\==V2.

consistants([X1,V1],[X2,V2]):-variable(VAR), valeurs(VAL),
member(X1,VAR),
member(X2,VAR),
member(V1,VAL),
member(V2,VAL),
V1==V2,
inscrits(X1,L1),
inscrits(X2,L2),
not(elements_identique(L1,L2)).
/*résultats des tests
?- consistants([lif1,1],[lif4,1]).
true .
?- inscrits(lif1,L1),inscrits(lif4,L2).
L1 = [marc, jean, paul, pierre],
L2 = [mark].
?- consistants([lif1,1],[lif2,2]).
true .
?- consistants([lif1,1],[lif2,1]).
false.
?- inscrits(lif1,L1),inscrits(lif2,L2).
L1 = [marc, jean, paul, pierre],
L2 = [marc, jean, anne, antoine].
*/

/*elements_identique(L1,L2)*/
/*vérifier si il existe les memes éléments dans L1 et L2*/
elements_identique([],[]).
elements_identique([X|_],L2):-member(X,L2).
elements_identique([X|L1],L2):-not(member(X,L2)),elements_identique(L1,L2).
/*résultats des tests
?- elements_identique([1],[1]).
true .
?- elements_identique([1],[1,2]).
true
?- elements_identique([3,4],[1,2,3,4]).
true .
*/

/*2. Méthode "générer et tester"*/
genereEtTeste(Solution):-variable(VAR),generer(VAR,Solution),tester(Solution).
/*résultats des tests
?- genereEtTeste(S).
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif4, 1], [lif5, 4], [lif6, 2]] .
*/

/*generer(VAR,R)*/
/*on met tous les cas possible de VAR dans R*/
generer([],[]):-!.
generer([UE|LVAR], [[UE,N]|L]):-valeurs(VAL),member(N,VAL), generer(LVAR,L).
/*résultats des tests
?- generer([lif1,lif2,lif3],R).
R = [[lif1, 1], [lif2, 1], [lif3, 1]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 2]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 3]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 4]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 5]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 6]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 7]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 8]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 9]] ;
R = [[lif1, 1], [lif2, 1], [lif3, 10]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 1]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 2]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 3]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 4]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 5]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 6]] ;
R = [[lif1, 1], [lif2, 2], [lif3, 7]] ;
....
 */

/*test(L)*/
/*on fais p_tester à partir de premier élément après on teste suivant*/
tester([]).
tester([X|L]):-p_tester(X,L), tester(L).

/*p_tester(X,L)*/
/*on teste couple X avec tous les autres couples de L*/
p_tester(_,[]).
p_tester(X,[Y|L]):-consistants(X,Y),p_tester(X,L).
/*
?- tester([[lif1,1],[lif2,2]]).
true .
?- tester([[lif1,1],[lif2,1]]).
false.
?- p_tester([lif1,1],[[lif2,2]]).
true .
?- p_tester([lif1,1],[[lif2,1],[lif2,2]]).
false.
 */

/*3. Méthode "retour arrière"*/
/*尾递归 (tail recursion)*/
/*generer2(Variable, Solution_Partielle, Solution)
Variable:entrée, une liste de variable
Solution_Partielle: entrée, le solution jusqu'à N intération
Solution:sortie, solution finale
 */
generer2([],Sp,Sp):-!.
generer2([UE|LVAR], Sp , S):-valeurs(VAL),member(N,VAL),tester([[UE,N]|Sp]),generer2(LVAR,[[UE,N]|Sp],S).

/*reverse(L1,L2)*/
/*
L1:Solution entrée à inversé
L2:Solution sortie inversé
 */
reverse([], []):-!.
reverse([[X,Y]|L1], L):-reverse(L1,L2),append(L2,[[X,Y]],L).

/*retourArriere(Solution)*/
retourArriere(Solution):-variable(VAR),generer2(VAR,[],SolutionI), reverse(SolutionI, Solution).

/*résultats des tests
?- retourArriere(S).
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif4, 1], [lif5, 4], [lif6, 2]] .
*/

/*4. Méthode filtrage*/
/*On utilise un "tail recursion" comme generer2 mais chaque intération on enleve tous les suites de domaines inconsistant*/
generer3([],Sp,Sp):-!.
generer3([[UE,VALS]|LD], Sp , S):-member(N,VALS), tester([[UE,N]|Sp]), write([[UE,VALS]|LD]), write("\n"), enleve_domaines(UE, N, LD, NLD),generer3(NLD,[[UE,N]|Sp],S).

filtrage(Solution):-domaines(Domaine),generer3(Domaine, [], SolutionI),reverse(SolutionI, Solution).
/*résultats des tests
?- filtrage(S).
[[lif1,[1,2,3,4,5,6,7,8,9,10]],[lif2,[1,2,3,4,5,6,7,8,9,10]],[lif3,[1,2,3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[1,2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif2,[2,3,4,5,6,7,8,9,10]],[lif3,[2,3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif3,[3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif5,[4,5,6,7,8,9,10]],[lif6,[2,3,4,5,6,7,8,9,10]]]
[[lif6,[2,3,4,5,6,7,8,9,10]]]
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif4, 1], [lif5, 4], [lif6, 2]] .
*/

/*enleve(X,L,L1): enleve X depuis liste L et retourne L1*/
enleve(_, [], []):-!.
enleve(X,[X|L],L):-!.
enleve(X,[Y|L],[Y|L1]):-X \== Y, enleve(X,L,L1).

/*résultats des tests
?- enleve(3,[1,2,3,4,5],L).
L = [1, 2, 4, 5] .
*/

/*enleve_domaines(VAR, VAL, Domaine, NewDomaine):enleve valeur N dans tous les domaines inconsistant et retourne le nouveau domaine*/
enleve_domaines(_, _, [], []):-!.
enleve_domaines(UE, N, [[UE1,VALS1]|LD], [[UE1,NVALS1]|NLD]):- not(consistants([UE,N],[UE1,N])), enleve(N, VALS1, NVALS1), enleve_domaines(UE, N, LD, NLD).
enleve_domaines(UE, N, [[UE1,VALS1]|LD], [[UE1,VALS1]|NLD]):- consistants([UE,N],[UE1,N]), enleve_domaines(UE, N, LD, NLD).

test(NL):-domaines([_|L]), enleve_domaines(lif1,2,L,NL).
/*résultats des tests
?- test(NL).
NL = [[lif2, [1, 3, 4, 5, 6, 7|...]], [lif3, [1, 3, 4, 5, 6|...]], [lif4, [1, 2, 3, 4|...]], [lif5, [1, 3, 4|...]], [lif6, [1, 2|...]]] .
*/

/*5. Méthode heuristique*/

generer4([],Sp,Sp):-!.
generer4([[UE,VALS]|LD], Sp , S):-min_premier_domaines([[UE,VALS]|LD],[[UE1,VALS1]|LD1]), write([[UE1,VALS1]|LD1]), write("\n"), member(N,VALS1), tester([[UE1,N]|Sp]), enleve_domaines(UE1, N, LD1, NLD), generer4(NLD,[[UE1,N]|Sp],S).

heuristique(Solution):-domaines(Domaine),generer4(Domaine, [], SolutionI),reverse(SolutionI, Solution).
/*résultats des tests
?- heuristique(S).
[[lif1,[1,2,3,4,5,6,7,8,9,10]],[lif2,[1,2,3,4,5,6,7,8,9,10]],[lif3,[1,2,3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[1,2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif2,[2,3,4,5,6,7,8,9,10]],[lif3,[2,3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif3,[3,4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif5,[3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif5,[4,5,6,7,8,9,10]],[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif4,[1,2,3,4,5,6,7,8,9,10]],[lif6,[1,2,3,4,5,6,7,8,9,10]]]
[[lif6,[2,3,4,5,6,7,8,9,10]]]
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif5, 4], [lif4, 1], [lif6, 2]]
*/

/*compter(L,Nombre)
retourne nombre d'événement dans la liste L
*/
compter([],0).
compter([_|L],N):-compter(L,NR), N is NR + 1.
/*résultats des tests
?- compter([1,2,3,4,5],N).
N = 5.
*/

/*min_domaines(Domaines,MinDomaines)
retourne le couple de domaine dont le nombre de valeur est plus petit
*/
min_domaines([[UE,VALS]], [UE,VALS]):-!.
min_domaines([[_,VALS]|L], [MinUE,MinVal]):-compter(VALS,NbrV), min_domaines(L,[MinUE,MinVal]), compter(MinVal, NbrMinVal), NbrMinVal < NbrV.
min_domaines([[UE,VALS]|L], [UE,VALS]):-compter(VALS,NbrV), min_domaines(L,[_,MinVal]), compter(MinVal, NbrMinVal), NbrMinVal >= NbrV.

test_min_domaines(MIN):-domaines([X|L]), enleve_domaines(lif1,2,L,NL), min_domaines([X|NL],MIN).
/*
̀?- test_min_domaines(MIN).
MIN = [lif2, [1, 3, 4, 5, 6, 7, 8|...]] ;
*/

/*on met le couple domaines dont le nombre de valeurs le plus petit à la premier place*/
min_premier_domaines(Domaines, [Min|DomainesSansMin]):-min_domaines(Domaines,Min), enleve(Min,Domaines, DomainesSansMin).
test_min_premier_domaines(MinPremierDomaine):-domaines([X|L]), enleve_domaines(lif1,2,L,NL), min_premier_domaines([X|NL],MinPremierDomaine).
/*
?- test_min_premier_domaines(M).
M = [[lif2, [1, 3, 4, 5, 6, 7|...]], [lif1, [1, 2, 3, 4, 5|...]], [lif3, [1, 3, 4, 5|...]], [lif4, [1, 2, 3|...]], [lif5, [1, 3|...]], [lif6, [1|...]]] .
*/

/*6. Comparaison des differentes méthodes

?- time(genereEtTeste(S)).
% 70,317 inferences, 0.015 CPU in 0.015 seconds (99% CPU, 4793413 Lips)
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif4, 1], [lif5, 4], [lif6, 2]] .


?- time(retourArriere(S)).
% 2,124 inferences, 0.000 CPU in 0.000 seconds (95% CPU, 4576689 Lips)
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif4, 1], [lif5, 4], [lif6, 2]] .

?- time(filtrage(S)).
% 3,552 inferences, 0.001 CPU in 0.001 seconds (98% CPU, 3849993 Lips)
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif4, 1], [lif5, 4], [lif6, 2]] .



?- time(heuristique(S)).
% 5,475 inferences, 0.002 CPU in 0.002 seconds (99% CPU, 3540117 Lips)
S = [[lif1, 1], [lif2, 2], [lif3, 3], [lif5, 4], [lif4, 1], [lif6, 2]] .
*/

trouver1(N):-findall(S, genereEtTeste(S), L), length(L, N).
trouver2(N):-findall(S, retourArriere(S), L), length(L, N).
trouver3(N):-findall(S, filtrage(S), L), length(L, N).
trouver4(N):-findall(S, heuristique(S), L), length(L, N).
/*7. stagiaire et entreprise*/

preferences(s1, [[1,e2],[2,e4],[2,e6]]).
preferences(s2, [[1,e2],[1,e5],[2,e6]]).
preferences(s3, [[1,e1],[2,e3],[3,e6]]).
preferences(s4, [[1,e6],[2,e3]]).
preferences(s5, [[1,e2],[2,e1],[3,e5]]).
preferences(s6, [[1,e6],[2,e4],[2,e2],[3,e5],[4,e1]]).

preferences(e1, [[1,s5],[1,s3],[2,s6]]).
preferences(e2, [[1,s2],[2,s5],[3,s1]]).
preferences(e3, [[1,s3],[1,s4]]).
preferences(e4, [[1,s6],[2,s1]]).
preferences(e5, [[1,s5],[2,s2],[3,s6]]).
preferences(e6, [[1,s1],[2,s4],[2,s6],[3,s2],[4,s3]]).

stagiaires([s1,s2,s3,s4,s5,s6]).
/*entreprises([e1,e2,e3,e4,e5,e6]).*/

domainesES(NewDomaine):-stagiaires(S), coupleES(S, Domaines), enleve_domainesES(Domaines, NewDomaine).

/*enleve_domainesES(Domaines, NewDomaines) on enleve les entreprise qui a se_preferer dans le domaines qui preferer pas ce entreprise à la premier place*/
enleve_domainesES([],[]).
enleve_domainesES([[S,[E|LE]]|LR], [[S,[E|LE1]]|LR1]):-se_preferer(S1, E1), E1 \== E,S1 \== S, enleve(E1, LE, LE1), enleve_domainesES(LR, LR1).
enleve_domainesES([S|LR], [S|LR1]):-enleve_domainesES(LR, LR1).
/*
?- domainesES(D),write(D).
[[s1,[e2,e4,e6]],[s2,[e2,e5,e6]],[s3,[e1,e3,e6]],[s4,[e6,e3]],[s5,[e2,e1,e5]],[s6,[e6,e4,e5,e1]]]
D = [[s1, [e2, e4, e6]], [s2, [e2, e5, e6]], [s3, [e1, e3, e6]], [s4, [e6, e3]], [s5, [e2, e1|...]], [s6, [e6|...]]] .
*/

/*liste_entreprise(Liste_Prefe, Liste_Valeurs)*/
liste_entreprise([], []).
liste_entreprise([[_,E]|LE], [E|LE1]):-liste_entreprise(LE, LE1).
/*
?- liste_entreprise([[1,e2],[2,e4],[2,e6]],L).
L = [e2, e4, e6].
*/

/*coupleES(Liste_stagiaire, list_couples)*/
coupleES([], []).
coupleES([X|LS],[[X,LE]|LER]):-preferences(X,L), liste_entreprise(L, LE), coupleES(LS, LER).

/*
?- coupleES([s2,s3],C).
C = [[s2, [e2, e5, e6]], [s3, [e1, e3, e6]]].

?- coupleES([s1,s2,s3],C).
C = [[s1, [e2, e4, e6]], [s2, [e2, e5, e6]], [s3, [e1, e3, e6]]].
*/

/*se_preferer(Stagiaire, Entreprise) S prefere E à la premier place en meme temps que E prefere S à la premier place */
se_preferer(S,E):-preferences(S,[[1,E]|_]),preferences(E,[[1,S]|_]).

/*find(X,Y,L,P) si X est avant que Y dans liste L alors retourne X, sinon retourne Y*/

find(X, _, [[_,X]|_], X).
find(_, Y, [[_,Y]|_], Y).
find(X, Y, [[_,Z]|L], N):- X \== Z, Y\== Z, find(X, Y, L, N).

/*true si I prefere J à K*/
prefere(I,J,K):-preferences(I,L),find(J,K,L,J).

/*
?- prefere(e1,s5,s1).
true .

?- prefere(e1,s5,s6).
true .

?- prefere(s1,e6,e4).
false.
*/


consistantsES([S1,E1],[S2,E2]):- S1 \== S2, E1 \== E2, not(prefere(S1,E2,E1)), not(prefere(E2,S1,S2)).
consistantsES([S1,E1],[S2,E2]):- S1 \== S2, E1 \== E2, not(prefere(S2,E1,E2)), not(prefere(E1,S2,S1)).
consistantsES([S1,E1],[S2,E2]):- S1 \== S2, E1 \== E2.
