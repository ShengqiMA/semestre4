/* recherche dans un graphe d'etats */
:- dynamic interdit/1.

/*recherche(EtatCourant,EtatFinal,ListeTaboue,ListeOp) tous donnes sauf ListeOp */
recherche(Ef,Ef,_,[]) :- !.
recherche(Ec,Ef,Le,[Op|Lop]) :- operateur(Ec,Op,Es),
							not(member(Es,Le)),
							not(interdit(Es)),
							write(Ec),write(' '),write(Op),write(' '),write(Es),nl,
							recherche(Es,Ef,[Es|Le],Lop).

resoudre(Sol) :- etatInitial(Ei),etatFinal(Ef),recherche(Ei,Ef,[Ei],Sol).
