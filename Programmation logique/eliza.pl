<<<<<<< HEAD

/* eliza */

eliza :- write('Bonjour, qu est ce qui vous amene ?'),nl,
 		lire_phrase(Phrase),
		elisa(Phrase).

elisa([bye]) :- !.
elisa(Lmots) :- pattern(Stimulus,Reponse),
				sous_liste(Stimulus,Lmots),
				ecrire_phrase(Reponse),
				lire_phrase(NewPhrase),
				elisa(NewPhrase).


sous_liste(L1,L2) :- append(L3,_,L2), append(_,L1,L3).


pattern([je,suis,X],[depuis,combien,de,temps,etes,vous,X,?]).
pattern([j,aime],[quelqu,un,d,autre,dans,votre,famille,aime,t,il,cela,?]).
pattern([je,me,sens,_],[ressentez,vous,souvent,cela,?]).
pattern([X],[pouvez,vous,me,parler,de,votre,X,?]):-important(X).
pattern(_,[continuez]).

important(pere).
important(mere).
important(fils).
important(fille).
important(frere).
important(soeur).

lire_phrase(Liste):-
	read(Phrase),
	name(Phrase,P),
	divise(P,Liste).

divise(Chaine,[MotL|Liste]) :-
		append(Mot,[32|Reste],Chaine),
		name(MotL,Mot),
		divise(Reste,Liste).
divise(DernierMot,[DernierMotL]):-
		name(DernierMotL,DernierMot).

ecrire_phrase([]) :- nl.
ecrire_phrase([X|L]) :- write(X), write(' '),ecrire_phrase(L).

/* tours de hanoi */
/* hanoi(Nbdisques,TigeDepart,TigeIntermediaire,TigeArrivee) donnes */
hanoi(1,D,_,A) :- !, write(D),write(' -> '),write(A),nl.
hanoi(N,D,I,A) :- Nm1 is N-1, hanoi(Nm1,D,A,I),
					hanoi(1,D,I,A),
					hanoi(Nm1,I,D,A).




/* recherche dans un graphe d'etats */
:- dynamic interdit/1.

/*recherche(EtatCourant,EtatFinal,ListeTaboue,ListeOp) tous donnes sauf ListeOp */
recherche(Ef,Ef,_,[]) :- !.
recherche(Ec,Ef,Le,[Op|Lop]) :- operateur(Ec,Op,Es),
							not(member(Es,Le)),
							not(interdit(Es)),
							write(Ec),write(' '),write(Op),write(' '),write(Es),nl,
							recherche(Es,Ef,[Es|Le],Lop).

resoudre(Sol) :- etatInitial(Ei),etatFinal(Ef),recherche(Ei,Ef,[Ei],Sol).


/* probleme du labyrinthe */

etatInitial(entree).
etatFinal(sortie).

interdit(minotaure).

operateur(E1,[E1,E2],E2) :- couloir(E1,E2).
operateur(E1,[E1,E2],E2) :- couloir(E2,E1).
=======

/* eliza */

eliza :- write('Bonjour, qu est ce qui vous amene ?'),nl,
 		lire_phrase(Phrase),
		elisa(Phrase).

elisa([bye]) :- !.
elisa(Lmots) :- pattern(Stimulus,Reponse),
				sous_liste(Stimulus,Lmots),
				ecrire_phrase(Reponse),
				lire_phrase(NewPhrase),
				elisa(NewPhrase).


sous_liste(L1,L2) :- append(L3,_,L2), append(_,L1,L3).


pattern([je,suis,X],[depuis,combien,de,temps,etes,vous,X,?]).
pattern([j,aime],[quelqu,un,d,autre,dans,votre,famille,aime,t,il,cela,?]).
pattern([je,me,sens,_],[ressentez,vous,souvent,cela,?]).
pattern([X],[pouvez,vous,me,parler,de,votre,X,?]):-important(X).
pattern(_,[continuez]).

important(pere).
important(mere).
important(fils).
important(fille).
important(frere).
important(soeur).

lire_phrase(Liste):-
	read(Phrase),
	name(Phrase,P),
	divise(P,Liste).

divise(Chaine,[MotL|Liste]) :-
		append(Mot,[32|Reste],Chaine),
		name(MotL,Mot),
		divise(Reste,Liste).
divise(DernierMot,[DernierMotL]):-
		name(DernierMotL,DernierMot).

ecrire_phrase([]) :- nl.
ecrire_phrase([X|L]) :- write(X), write(' '),ecrire_phrase(L).

/* tours de hanoi */
/* hanoi(Nbdisques,TigeDepart,TigeIntermediaire,TigeArrivee) donnes */
hanoi(1,D,_,A) :- !, write(D),write(' -> '),write(A),nl.
hanoi(N,D,I,A) :- Nm1 is N-1, hanoi(Nm1,D,A,I),
					hanoi(1,D,I,A),
					hanoi(Nm1,I,D,A).




/* recherche dans un graphe d'etats */
:- dynamic interdit/1.

/*recherche(EtatCourant,EtatFinal,ListeTaboue,ListeOp) tous donnes sauf ListeOp */
recherche(Ef,Ef,_,[]) :- !.
recherche(Ec,Ef,Le,[Op|Lop]) :- operateur(Ec,Op,Es),
							not(member(Es,Le)),
							not(interdit(Es)),
							write(Ec),write(' '),write(Op),write(' '),write(Es),nl,
							recherche(Es,Ef,[Es|Le],Lop).

resoudre(Sol) :- etatInitial(Ei),etatFinal(Ef),recherche(Ei,Ef,[Ei],Sol).


/* probleme du labyrinthe */

etatInitial(entree).
etatFinal(sortie).

interdit(minotaure).

operateur(E1,[E1,E2],E2) :- couloir(E1,E2).
operateur(E1,[E1,E2],E2) :- couloir(E2,E1).
>>>>>>> a28117bae1411b88185b476133546957b7e2314b
