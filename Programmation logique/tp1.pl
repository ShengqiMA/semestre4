
/*exo 5*/

/*longueur*/
longueur([],0).
longueur([_|L],N):-longueur(L,M), N is M+1.
/*concat (r����crire append)*/
concat([],L,L).
concat([X|L1],L2,[X|L3]):-concat(L1,L2,L3).
/*palindrome(L)*/
palindrome([]).
palindrome([_]).
palindrome([X|L]):-concat(L1,[X],L),palindrome(L1).
/*rang_paire(X,Y)*/
rang_paire([],[]).
rang_paire([_],[]).
rang_paire([_,Y|L],[Y|L1]):-rang_paire(L,L1).
/*indice(X,L,N)*/
indice(_,[],-1).
indice(X,[X|_],1).
indice(X,[_|L],N):-indice(X,L,M),N is M+1.
/*remplace(X1,X2,L1,L2)*/
remplace(X1,X2,[X1|L],[X2|L]).
remplace(X1,X2,[Y|L1],[Y|L2]):-remplace(X1,X2,L1,L2).
/*somme(L,R)*/
somme(L,R):-sommebis(L,1,R).
sommebis([],_,0).
sommebis([X|L],I,R):-J is I+1,sommebis(L,J,S),R is S+I*X.
/*partage(L,X,L1,L2)*/
partage([],_,[],[]).
partage([Y|L],X,[Y|L1],L2):-Y<X,partage(L,X,L1,L2).
partage([Y|L],X,L1,[Y|L2]):-Y>=X,partage(L,X,L1,L2).
=======
/*exo 5*/

/*longueur*/
longueur([],0).
longueur([_|L],N):-longueur(L,M), N is M+1.
/*concat (r����crire append)*/
concat([],L,L).
concat([X|L1],L2,[X|L3]):-concat(L1,L2,L3).
/*palindrome(L)*/
palindrome([]).
palindrome([_]).
palindrome([X|L]):-concat(L1,[X],L),palindrome(L1).
/*rang_paire(X,Y)*/
rang_paire([],[]).
rang_paire([_],[]).
rang_paire([_,Y|L],[Y|L1]):-rang_paire(L,L1).
/*indice(X,L,N)*/
indice(_,[],-1).
indice(X,[X|_],1).
indice(X,[_|L],N):-indice(X,L,M),N is M+1.
/*remplace(X1,X2,L1,L2)*/
remplace(X1,X2,[X1|L],[X2|L]).
remplace(X1,X2,[Y|L1],[Y|L2]):-remplace(X1,X2,L1,L2).
/*somme(L,R)*/
somme(L,R):-sommebis(L,1,R).
sommebis([],_,0).
sommebis([X|L],I,R):-J is I+1,sommebis(L,J,S),R is S+I*X.
/*partage(L,X,L1,L2)*/
partage([],_,[],[]).
partage([Y|L],X,[Y|L1],L2):-Y<X,partage(L,X,L1,L2).
partage([Y|L],X,L1,[Y|L2]):-Y>=X,partage(L,X,L1,L2).
