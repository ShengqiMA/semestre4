
/*1.tri bulles*/
/*bulle(L1,L2)*/
bulle([],[]).
bulle([X],[X]).
bulle([X|L],[X|L]):-bulle(L,[Y|_]),X=<Y.
bulle([X|L],[Y,X|L2]):-bulle(L,[Y|L2]),X>Y.

/*tribulle(L1,L2)*/
tribulle([],[]).
tribulle([X],[X]).
tribulle(L1,[X|L2]):-bulle(L1,[X|L]),tribulle(L,L2).

/*2.arbres binaires*/
/*appartenance(X,ARB)*/
appartenance(X,[X,_,_]).
appartenance(X,[Y,G,_]):- X=\=Y, appartenance(X,G).
appartenance(X,[Y,_,D]):- X=\=Y, appartenance(X,D).

/* donne un parcours prefixe ǰ׺*/
element1(X,[X,_,_]).
element1(X,[Y,G,_]) :- X\==Y, element1(X,G).
element1(X,[Y,_,D]) :- X\==Y, element1(X,D).

/* donne un parcours infixe ��׺*/
element2(X,[Y,G,_]) :- X\==Y, element2(X,G).
element2(X,[X,_,_]).
element2(X,[Y,_,D]) :- X\==Y, element2(X,D).

/* donne un parcours postfixe ��׺*/
element3(X,[Y,G,_]) :- X\==Y, element3(X,G).
element3(X,[Y,_,D]) :- X\==Y, element3(X,D).
element3(X,[X,_,_]).


/*feuille(ARB,L)*/
/*
concat([],L,L).
concat([X|L1],L2,[X|L3]):-concat(L1,L2,L3).

feuille([],[]).
feuille([X,[],[]],[X]).
feuille([_,G,[]],L):-G=\=[],feuille(G,L).
feuille([_,[],D],L):-D=\=[],feuille(D,L).
feuille([_,G,D],L):-G=\=[],D=\=[],feuille(G,L1),feuille(D,L2),concat(L1,L2,L).*/
/* liste des feuilles d'un arbre */
/* feuilles(A,L) A arbre donne, L liste resultat */
feuilles([],[]).
feuilles([N,[],[]],[N]).
feuilles([_,G,[]],L) :-G\==[], feuilles(G,L).
feuilles([_,[],D],L) :-D\==[], feuilles(D,L).
feuilles([_,G,D],L) :- G\==[], D\==[], feuilles(G,L1),feuilles(D,L2), append(L1,L2,L).

/*feuillesR(ARB,L)*/
feuillesR([],[]).
feuillesR([X,[],[]],[X]).
feuillesR([_,G,[]],L) :- G\==[], feuillesR(G,L).
feuillesR([_,[],D],L) :- D\==[], feuillesR(D,L).
feuillesR([_,G,D],L) :- D\==[], G\==[], feuillesR(G,L1), feuillesR(D,L2), append(L1,L2,L).
/* un predicat "feuille(A)" vrai si A est une feuille permettrait de simplifier ce predicat */

/*3.Arbres binaires de recherche*/
/*insertion(X,A1,A2)*/
insertion(X,[],[X,[],[]]).
insertion(X,[N,G,D],[N,A,D]):-X<N,insertion(X,G,A).
insertion(X,[N,G,D],[N,G,A]):-X>=N,insertion(X,D,A).

/* calcul et suppression du maximum : suprmax(ABR donne, nb resultat,ABR resultat ) */
/*supmax(ARB,MAX,P)*/
supmax([N,G,[]],N,G).
supmax([N,G,D],MAX,[N,G,D1]):-D\==[],supmax(D,MAX,D1).

/*sup(X,A,A1) */
sup(_,[],[]).
sup(X,[N,G,D],[N,G1,D]):-X<N, sup(X,G,G1).
sup(X,[N,G,D],[N,G,D1]):-X>N, sup(X,D,D1).
sup(X,[X,[],D],D).
sup(X,[X,G,[]],G):-G\==[].
sup(X,[X,G,D],[MAX,G1,D]):-G\==[],D\==[],supmax(G,MAX,G1).

/*construire(L,A)*/
construire([],[]).
construire([X|L],A):-construire(L,A1),insertion(X,A1,A).
/* tri d'une liste via la construction d'un ABR */
/*��һ�����б��������������ٱ��������Ķ���*/
/* L liste de nb donnee, L1 liste de nb triee resultat */
triarbre(L,L1) :- construire(L,A), infixe(A,L1).

/* liste (resultats) du parcours infixe d'un arbre (donne) */
infixe([],[]).
infixe([N,G,D],L) :- infixe(G,L1), infixe(D,L2), append(L1,[N|L2],L).

/*4.Ensembles*/
/*to_list(X,Y)*/
to_list([],[]).
to_list([X|L],[X|L2]) :- elimine_tout(L,X,L1), to_list(L1,L2).

elimine_tout([],_,[]).
elimine_tout([X|L],X,L1) :- elimine_tout(L,X,L1).
elimine_tout([Y|L],X,[Y|L1]) :- X\==Y, elimine_tout(L,X,L1).

/* deux listes-ensembles donnees */
inclus([],_).
inclus([X|L],L1) :- member(X,L1), inclus(L,L1).

/* L1 L2 deux listes-ensembles donnees, L3 resultat */
/*findall(X,ν��(X),L) L������������ν�ʵ�X*/
inter(L1,L2,L3) :- findall(X,(member(X,L1),member(X,L2)),L3).

inter2([],_,[]).
inter2([X|L1],L2,[X|L3]) :- member(X,L2), inter2(L1,L2,L3).
inter2([X|L1],L2,L3) :- not(member(X,L2)), inter2(L1,L2,L3).
=======
/*1.tri bulles*/
/*bulle(L1,L2)*/
bulle([],[]).
bulle([X],[X]).
bulle([X|L],[X|L]):-bulle(L,[Y|_]),X=<Y.
bulle([X|L],[Y,X|L2]):-bulle(L,[Y|L2]),X>Y.

/*tribulle(L1,L2)*/
tribulle([],[]).
tribulle([X],[X]).
tribulle(L1,[X|L2]):-bulle(L1,[X|L]),tribulle(L,L2).

/*2.arbres binaires*/
/*appartenance(X,ARB)*/
appartenance(X,[X,_,_]).
appartenance(X,[Y,G,_]):- X=\=Y, appartenance(X,G).
appartenance(X,[Y,_,D]):- X=\=Y, appartenance(X,D).

/* donne un parcours prefixe ǰ׺*/
element1(X,[X,_,_]).
element1(X,[Y,G,_]) :- X\==Y, element1(X,G).
element1(X,[Y,_,D]) :- X\==Y, element1(X,D).

/* donne un parcours infixe ��׺*/
element2(X,[Y,G,_]) :- X\==Y, element2(X,G).
element2(X,[X,_,_]).
element2(X,[Y,_,D]) :- X\==Y, element2(X,D).

/* donne un parcours postfixe ��׺*/
element3(X,[Y,G,_]) :- X\==Y, element3(X,G).
element3(X,[Y,_,D]) :- X\==Y, element3(X,D).
element3(X,[X,_,_]).


/*feuille(ARB,L)*/
/*
concat([],L,L).
concat([X|L1],L2,[X|L3]):-concat(L1,L2,L3).

feuille([],[]).
feuille([X,[],[]],[X]).
feuille([_,G,[]],L):-G=\=[],feuille(G,L).
feuille([_,[],D],L):-D=\=[],feuille(D,L).
feuille([_,G,D],L):-G=\=[],D=\=[],feuille(G,L1),feuille(D,L2),concat(L1,L2,L).*/
/* liste des feuilles d'un arbre */
/* feuilles(A,L) A arbre donne, L liste resultat */
feuilles([],[]).
feuilles([N,[],[]],[N]).
feuilles([_,G,[]],L) :-G\==[], feuilles(G,L).
feuilles([_,[],D],L) :-D\==[], feuilles(D,L).
feuilles([_,G,D],L) :- G\==[], D\==[], feuilles(G,L1),feuilles(D,L2), append(L1,L2,L).

/*feuillesR(ARB,L)*/
feuillesR([],[]).
feuillesR([X,[],[]],[X]).
feuillesR([_,G,[]],L) :- G\==[], feuillesR(G,L).
feuillesR([_,[],D],L) :- D\==[], feuillesR(D,L).
feuillesR([_,G,D],L) :- D\==[], G\==[], feuillesR(G,L1), feuillesR(D,L2), append(L1,L2,L).
/* un predicat "feuille(A)" vrai si A est une feuille permettrait de simplifier ce predicat */

/*3.Arbres binaires de recherche*/
/*insertion(X,A1,A2)*/
insertion(X,[],[X,[],[]]).
insertion(X,[N,G,D],[N,A,D]):-X<N,insertion(X,G,A).
insertion(X,[N,G,D],[N,G,A]):-X>=N,insertion(X,D,A).

/* calcul et suppression du maximum : suprmax(ABR donne, nb resultat,ABR resultat ) */
/*supmax(ARB,MAX,P)*/
supmax([N,G,[]],N,G).
supmax([N,G,D],MAX,[N,G,D1]):-D\==[],supmax(D,MAX,D1).

/*sup(X,A,A1) */
sup(_,[],[]).
sup(X,[N,G,D],[N,G1,D]):-X<N, sup(X,G,G1).
sup(X,[N,G,D],[N,G,D1]):-X>N, sup(X,D,D1).
sup(X,[X,[],D],D).
sup(X,[X,G,[]],G):-G\==[].
sup(X,[X,G,D],[MAX,G1,D]):-G\==[],D\==[],supmax(G,MAX,G1).

/*construire(L,A)*/
construire([],[]).
construire([X|L],A):-construire(L,A1),insertion(X,A1,A).
/* tri d'une liste via la construction d'un ABR */
/*��һ�����б��������������ٱ��������Ķ���*/
/* L liste de nb donnee, L1 liste de nb triee resultat */
triarbre(L,L1) :- construire(L,A), infixe(A,L1).

/* liste (resultats) du parcours infixe d'un arbre (donne) */
infixe([],[]).
infixe([N,G,D],L) :- infixe(G,L1), infixe(D,L2), append(L1,[N|L2],L).

/*4.Ensembles*/
/*to_list(X,Y)*/
to_list([],[]).
to_list([X|L],[X|L2]) :- elimine_tout(L,X,L1), to_list(L1,L2).

elimine_tout([],_,[]).
elimine_tout([X|L],X,L1) :- elimine_tout(L,X,L1).
elimine_tout([Y|L],X,[Y|L1]) :- X\==Y, elimine_tout(L,X,L1).

/* deux listes-ensembles donnees */
inclus([],_).
inclus([X|L],L1) :- member(X,L1), inclus(L,L1).

/* L1 L2 deux listes-ensembles donnees, L3 resultat */
/*findall(X,ν��(X),L) L������������ν�ʵ�X*/
inter(L1,L2,L3) :- findall(X,(member(X,L1),member(X,L2)),L3).

inter2([],_,[]).
inter2([X|L1],L2,[X|L3]) :- member(X,L2), inter2(L1,L2,L3).
inter2([X|L1],L2,L3) :- not(member(X,L2)), inter2(L1,L2,L3).
>>>>>>> a28117bae1411b88185b476133546957b7e2314b
