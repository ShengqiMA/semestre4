1,
typeof "John"
'string'
typeof 3.14
'number'
typeof NaN
'number'
typeof false
'boolean'
typeof null
'object'
typeof undefined
'undefined'
typeof un_truc_pas_declare
'undefined'
typeof{}
'object'
typeof{name:"John", age:34}
'object'
({}instanceof Object) 
true
typeof[1,2,3,4]
'object'
[1,2] instanceof Array
true
[1,2] instanceof Object
true
typeof new Date()
'object'
typeof new Date("2020-02-02")
'object'
new Date() instanceof Date
true
new Date() instanceof Object
true
typeof function(){}
'function'
typeof(()=>{})
'function'
(() =>{}) instanceof Function
true
(() =>{}) instanceof Object
true
//
//typeof
typeof用以获取一个变量或者表达式的类型，typeof一般只能返回如下几个结果：

number

boolean

string

function（函数）

object（NULL,数组，对象）

undefined

instanceof
instanceof用于判断一个变量是否某个对象的实例
(param1, param2, …, paramN) => { statements }
(param1, param2, …, paramN) => expression
//相当于：(param1, param2, …, paramN) =>{ return expression; }

// 当只有一个参数时，圆括号是可选的：
(singleParam) => { statements }
singleParam => { statements }

// 没有参数的函数应该写成一对圆括号。
() => { statements }
2.
