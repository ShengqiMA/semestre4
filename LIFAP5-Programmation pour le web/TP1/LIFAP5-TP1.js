"use strict";

/* Exercice 0 : Tutoriel */
function iterate_array(arr){
  /* TODO */
  for(var i = 0; i < arr.length;i++){
    console.log(arr[i]);
  }
  console.log("------------------");
  i = 0;
  while(i < arr.length){
    console.log(arr[i]);
    i++;
  }
  console.log("------------------");
  for(let i of arr){
    console.log(i);
  }
  console.log("------------------");
  for(let i in arr){
    console.log(i);
  }
  console.log("------------------");
  arr.forEach(function(element){console.log(element)});
}

function test(){
  iterate_array([1,2,3,6,5,4]);
  iterate_array([{a:0, b:1}, {a:1, b:42}]);
  console.log(fibonacci(10));
  if (typeof my_object.do !== "undefined")
    my_object.do();
}

let my_object = {
  x:4,
  y:3,
  do(){
    console.log(this.x,this.y)
  }
};

function fibonacci(n){
  /* TODO */
  var fib = [0,1];
  for(let i = 2; i < n;i++){
    fib.push(fib[i-1] + fib[i-2]);
  }
  return fib;
}

function fibonacci_rec(n){
  /* TODO */
  if(Number.isInteger(n)){
    return fibonacci(n);
  }
}


/* Exercice 1 : 99 Bottles of Beer */
function bottles(beers){
  console.log('[js] bottles (' + beers +')');
  let res = "";
  res += '[js] bottles (' + beers +')';
  /* TODO */

  return res;
}
/* Exercice 2 : fonction range */
function range(stop, start, step) {
  console.log('[js] range(' + stop  + ',' + start  +',' + step + ')');
  var res = [];
  if(start <= stop){
      for(var i = start; i <= stop;i+=step){
        res.push(i);
      }
  }

  /* TODO */

  console.log('[js] range(' + stop + ',' + start +',' + step + ')=' + res);
  return res;
}

function rangerec(stop, start, step) {
  if(stop >= start){
      var res = [start];
      console.log(res);
      res.concat(rangerec(stop, start+step, step));
      return res;
  }

}



/* Exercice 3 : Calculatrice polonaise inverse */
let op = function(x, y, o){
  if(o == "+") return x+y;
  if(o == "-") return x-y;
  if(o == "*") return x*y;
  if(o == "/") return x/y;
}
let evaluate = function(expr) {
  console.log('[js] evaluate (' + expr +')');
  let results = [];
  let temp = [];
  //results = expr.split(" ");
  for(let i = 0;i < expr.length;i+=2){
    if(!isNaN(expr[i])) results.push(parseInt(expr[i]));
    else results.push(expr[i]);
  }
  console.log(results);
  for(var i in results){
    if(!isNaN(results[i])){
      temp.push(results[i]);
    }else{
      let y = temp.pop();
      let x = temp.pop();
      let t = op(x, y, results[i]);
      temp.push(t);
      console.log(t);
    }
  }
  console.log(temp);
  /* TODO */

  return temp.pop() || 0;
}

document.addEventListener('DOMContentLoaded', function(){

   /* Exercice 1 : 99 Bottles of Beer */
   let output1  = document.getElementById("output1");
   let input1   = document.getElementById("input1");

   document.getElementById("eval1").onclick = function() {
    output1.innerHTML = bottles(input1.value);
  };

   /* Exercice 2 : fonction range */
  let output2     = document.getElementById("output2");
  let input2stop  = document.getElementById("input2stop");
  let input2start = document.getElementById("input2start");
  let input2step  = document.getElementById("input2step");

  document.getElementById("reset2").onclick = function(){
    input2stop.value  = 10;
    input2start.value = "";
    input2step.value  = "";
    output2.innerHTML = "";
  };

  document.getElementById("eval2").onclick = function() {
    let stop  = Number(input2stop.value);
    let start = (input2start.value!=="")?Number(input2start.value):undefined;
    let step  = (input2step.value!=="")?Number(input2step.value):undefined;

    output2.innerHTML += '[' + String(range(stop, start, step))+']<br>';
  };

   /* Exercice 3 : Calculatrice polonaise inverse */
  let output3 = document.getElementById("output3");
  let input3  = document.getElementById("input3");

  document.getElementById("reset3").onclick = function(){
    output3.innerHTML = "";
  };

  document.getElementById("eval3").onclick = function() {
    let res = evaluate(input3.value);
    output3.innerHTML += (String(res) + '<br>');
  };

}, false);
