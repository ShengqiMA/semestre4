// Bonne pratique: le javascript doit s'exécuter une fois la page HTML chargée
document.addEventListener('DOMContentLoaded', function(){ 
    console.log('début du code js');

    // Copier le titre dans la balise h1
    document.getElementById("titreDansTexte").innerHTML = 
	document.getElementById("titreDansHead").innerHTML;


    // Fonction générique qui fabrique une fonction anonyme.
    // La fonction anonyme applique un opérateur binaire i.e. une
    // fonction à 2 arguments sur le contenu des champs "resultat" et
    // "saisie", et place le résultat dans le champs "resultat".
    let applique = function(op) {
	return function() { // cette fonction est fabriquée et
	                    // renvoyée lors de l'appel à applique
	    let x = Number(document.getElementById("resultat").innerHTML);
	    let y = Number(document.getElementById("saisie").value);
	    let res = op(x,y);
	    document.getElementById("resultat").innerHTML = String(res);
	}
    }
    
    // Gestion du bouton +
    document.getElementById("plus").onclick = applique((x,y) => x+y);

    // Gestion du bouton -
    document.getElementById("moins").onclick = applique((x,y) => x-y);

    // Gestion du bouton *
    document.getElementById("mult").onclick = applique((x,y) => x*y);

    // Gestion du bouton /
    document.getElementById("div").onclick = applique((x,y) => x/y);
    
    
    // Gestion de la remise à zéro
    let raz = function() {
	// Mettre le résultat à 0
	document.getElementById("resultat").innerHTML = "0";
	// Vider le champ de saisie
	document.getElementById("saisie").value = "";
    }
    document.getElementById("reset").onclick = raz;
    
    raz();

    console.log("fin du code js");
}, false);

