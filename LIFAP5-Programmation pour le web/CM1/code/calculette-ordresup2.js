// Bonne pratique: le javascript doit s'exécuter une fois la page HTML chargée
document.addEventListener('DOMContentLoaded', function(){ 
    console.log('début du code js');
    
    // Copier le titre dans la balise h1
    document.getElementById("titreDansTexte").innerHTML =
        document.getElementById("titreDansHead").innerText;
   
    // Gestion des opérations
    const bouton_op = function(nomBouton, op) {
        const calcule = function() {
            const x = 
                Number(document.getElementById("resultat").innerText);
            const y = 
                Number(document.getElementById("saisie").value);
            document.getElementById("resultat").innerText = op(x,y);
        }
        document.getElementById(nomBouton).onclick = calcule; 
    };

    // Gestion du bouton +
    bouton_op("plus", function(x,y){
        return x+y;
    });

    // Gestion du bouton -
    bouton_op("moins", (x,y) => x-y);
    
    // Gestion du bouton *
    bouton_op("mult", (x,y) => x*y);

    // Gestion du bouton /
    bouton_op("div", (x,y) => x/y);
    
    // Gestion de la remise à zéro
    const raz = function() {
        document.getElementById("resultat").innerText = "0";
        document.getElementById("saisie").value = "0";
    }
    document.getElementById("reset").onclick = raz;
  
    raz();

    console.log("fin du code js");
}, false);

