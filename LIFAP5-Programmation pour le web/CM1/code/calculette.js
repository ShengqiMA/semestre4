// Bonne pratique: le javascript doit s'exécuter une fois la page HTML chargée
document.addEventListener('DOMContentLoaded', function(){ 
    console.log('début du code js');

    // Copier le titre dans la balise h1
    document.getElementById("titreDansTexte").innerHTML = 
	document.getElementById("titreDansHead").innerHTML;

    // Vider le champ de saisie
    document.getElementById("saisie").value = "";
    
    // Gestion du bouton +
    let plus = function() {
	let x = Number(document.getElementById("resultat").innerHTML);
	let y = Number(document.getElementById("saisie").value);
	let res = x + y;
	document.getElementById("resultat").innerHTML = String(res);
    };
    document.getElementById("plus").onclick = plus;

    // Gestion du bouton -
    let moins = function() {
	let x = Number(document.getElementById("resultat").innerHTML);
	let y = Number(document.getElementById("saisie").value);
	let res = x - y;
	document.getElementById("resultat").innerHTML = String(res);
    };
    document.getElementById("moins").onclick = moins;

    // Gestion du bouton *
    let mult = function() {
	let x = Number(document.getElementById("resultat").innerHTML);
	let y = Number(document.getElementById("saisie").value);
	let res = x * y;
	document.getElementById("resultat").innerHTML = String(res);
    };
    document.getElementById("mult").onclick = mult;

    // Gestion du bouton /
    let div = function() {
	let x = Number(document.getElementById("resultat").innerHTML);
	let y = Number(document.getElementById("saisie").value);
	let res = x / y;
	document.getElementById("resultat").innerHTML = String(res);
    };
    document.getElementById("div").onclick = div;
    
    // Gestion de la remise à zéro
    let raz = function() {
	document.getElementById("resultat").innerHTML = "0";
    }
    document.getElementById("reset").onclick = raz;

    console.log("fin du code js");
}, false);

