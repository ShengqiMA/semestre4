# ES6 support
http://kangax.github.io/compat-table/es6/

# Programmation concurrente
Montrer la console réseau de la page des masters: http://master-info.univ-lyon1.fr/TIW#planning

# Types

	typeof 1
	typeof 1.3
	typeof true
	typeof "toto"
	typeof 'titi'
	typeof null
	typeof undefined
	typeof Infinity
	

# Expressions

	"toto" + 'titi'
	true || false
	2.5 + 3 * 4
	"toto" > 'titi'
	2 < 3.5

# Variables et constantes
## Portée des constantes

	for(let i = 0; i < 3; ++i) {
	  {
	    const test = i;
	    console.log(test);
	  }
	  console.log(i);
	}

## Tests sur variables

	x = null;
	x == undefined

	x === undefined


# Structures de contrôle

## Switch

    x = "toto"
	switch (x) {
	case "titi":
		console.log(1);
		break;
	case "tutu":
		console.log(2);
		break;
	case "toto":
		console.log(3);
		break;
	default:
		console.log(4);
	}

# Définitions de fonctions

	let ajoute3 = function(x) {
		return x+3;
	};
	ajoute3(5);
	
	ajoute3

# Objets
## Création

	let o1 = { x: 5, y: 3 };
	o1;

	let o2 = { "x": 5, "y": 3};
	o2;

## Accès aux champs
### Notation pointée
	o1.x;

	o1.x = 12;
	o1.x;
	
	o1.z = 3;
	o1;

### Notation crochets

	o1["x"];

	let c = "to"+"ti";
	o1[c] = 18;
	o1;

	
## Méthodes

	let o3 = {
		salut: function() {
			console.log("Bonjour");
		}
	}
			
	let o4 = {
		salut: function() {
			console.log("Salut");
		}
	}		

	o3.salut;

	o3.salut();

	o4.salut();
	
## This

	let p = {
		x: 5,
		y: 3,
		deplace: function(dx, dy) {
			this.x = this.x + dx;
			this.y = this.y + dy;
		}
	};
	p.x

	p.deplace(1,2);

	p.x

## Fabrique

	let Point = function(x_init, y_init) {
		return {
			x: x_init,
			y: y_init,
			deplace: function(dx, dy) {
				this.x = this.x + dx;
				this.y = this.y + dy;
			}
		};
	}

	let p2 = Point(4,6);
	p2.x;

	p2.deplace(3,4);
	p2.y

## Constructeur

	let Point2 = function(x_init, y_init) {
		this.x = x_init;
		this.y = y_init;
		this.deplace = function(dx, dy) {
			this.x = this.x + dx;
			this.y = this.y + dy;
		};
	}

	let p3 = new Point2(4,6);
	p3.x;

	p3.deplace(3,4);
	p3.y

## Tableaux

	t = Array(1,2,3);
	t

	t = [1,2,3];
	t

	t[0]

	t.length

	t.push(12)

	t[7] = 18
	t

## Itérations

	for(let v of t) {
		console.log(v);
	}
		
	for(let c in p1) {
		console.log(c);
	}

	for(let c in p1) {
		console.log(p1[c]);
	}

	for(let i in t) {
		console.log(i);
	}

	for(let i in t) {
		console.log(t[i]);
	}

## Références sur des objets

	let x = 3;
	let y = x;
	x = 5;
	y

	let o5 = { a: 3 };
	let o6 = o5;
	o5.a = 5;
	o6

## JSON

	let adresse = function(n, r, v) {
		return {
			numero: n,
			rue: r,
			ville: v,
			affiche: function() { console.log(this); }
		};
	}

	let a1 = adresse(23, "Pierre de Coubertin", "Villeurbanne");
	let a2 = adresse(undefined, "Ada Byron", "Villeurbanne");
	let p = {
		nom: "EC",
		adresses : []
	}
	p.adresses.push(a1);
	p.adresses.push(a2);

	for(let adr of p.adresses) {
		adr.affiche();
	}

	jsonVal = JSON.stringify(p);

	let p2 = JSON.parse(jsonVal);

	p.adresses[0].affiche

	p2.adresses[0].affiche

# JS Quirks

## Conversions

	'5'*3

	'5' + 3

	'5' + 3 * '2'

	5 + 3 * '2'

	true + true

## Comparaisons

	0 == false
	'0' == false
	'1' == true
	'2' == true
	"  123  \n" == 123

	'0' == false
	false == ''
	'0' == ''

	a = {x:5};
	b = {x:5};
	c = a;

	a == b
	a == c
