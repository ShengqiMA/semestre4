1.
var x 全局变量
let y 局部变量
const c 局部常量

2.
x === undefined // x non dé fini
x == null // x null ou undefined

3.
//Rangée dans une variable
let maFonction = function (arg1 , arg2 ) {
/* ... */
return uneValeur ;
};

4.对象
大括号{}表示定义一个对象
对象中可以又成员变量和函数

let monObjet = { a: 5, b: " toto " };
let monObjet = { "b": " toto ", "a": 5 };
一般情况下加不加双引号是一样的除非:
  1.成员变量名为数字开头或者纯数字
  2.成员变量名中间有-相连
  以上情况时，访问该成员变量必须为[]不能用.

访问: monObjet.a monObjet.b 或 monObjet["a"] monObjet["b"]
var LangShen = {
Name = function(){ return "LangShen"; },
Age = function(){ return "28"; }
}//定义了一个包含两个成员函数的对象
使用：
alert( LangShen.Name() );

5.
