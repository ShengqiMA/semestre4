#include <iostream>
#include <string>

//strerror
#include <errno.h>

//read write close
#include <unistd.h>

//open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

#define LEN 16
int main(int argc, char * argv[]){

  if(argc > 2){
    cerr << "Il faut exactement un argument après la commande" << endl;
    return EXIT_FAILURE;
  }
  int fd = open(argv[1], O_RDONLY);
  if(fd == -1){
    cerr << "Impossible d'ouvirir" << argv[1] << "en lecture" << endl;
    return EXIT_FAILURE;
  }
  char buf[LEN];
  std::string s;
  int nbrd = read(fd, buf, LEN);
  while(nbrd > 0){
    int nbwr = 0;
    int nbr = nbrd;
    while(nbr > 0){
        int nb = write(STDOUT_FILENO, buf+nbwr, nbr);
        if(nb < 0){
          cerr << "error quand on écrire" << endl;
          return 1;
        }
        nbwr += nb;
        nbr -= nb;
    }
    nbrd = read(fd, buf, LEN);
  }
  if(nbrd < 0){
    cerr << "error quand on lire" << endl;
    return 1;
  }
  close(fd);
  return 0;
}
