#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>

#include "socklib.h"

using namespace std;

// Affiche l'éventuel message d'erreur passé en argument,
// suivi du message donné par la variable errno.
void print_error(const string &msg = "");

// Affiche l'éventuel message d'erreur passé en argument,
// avec print_error(), puis termine le processus appelant
// avec pour valeur de retour EXIT_FAILURE.
void exit_error(const string &msg = "");


void print_usage(char *name) {
  cout << "usage: " << name << " client host port" << endl;
}

int main(int argc, char *argv[]) {
  char *host = NULL;
  char *port = NULL;

  if(argc < 3) {
    print_usage(argv[0]);
    return(1);
  }

  host = argv[1];
  port = argv[2];

    // TODO1
    int sd = create_client_socket(host, port);
    if(sd == -1) exit_error("create_client_socket");


    cout << "Logiquement, on est connecté au serveur." << endl;

    string line;
    int res;
    while(1) {
      res = recv_line(sd, line);
      if(res == -1) {
        print_error();
        break;
      }

      if(res == 0) {
        cout << "Connexion fermée du côté du client..." << endl;
        break;
      }
      cout << line << endl;
      // Réception d'une ligne sur la socket, puis affichage.
      // TODO2
      // Lecture d'une ligne sur l'entrée standard, et envoi sur la socket.
      getline(cin, line); // ATTENTION, getline() *enlève* le '\n' de fin de ligne !

      // TODO3
      if(line == "quit") {
        send_str(sd, line);
        break;
      }
      // TODO4
      line.push_back('\n');
      send_str(sd, line);
    }

    cout << "Fermeture de la connexion." << endl;

    close(sd);

    return(0);
  }

  void print_error(const string &msg) {
    if(msg == "") cerr << "Erreur : " << strerror(errno) << endl;
    else cerr << "Erreur, " << msg << " : " << strerror(errno) << endl;
  }

  void exit_error(const string &msg) {
    print_error(msg);
    exit(EXIT_FAILURE);
  }
