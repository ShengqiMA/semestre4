#include <iostream>
using namespace std;

boo quit = false;

void handler(int signal){
  quit = true;
}

int main(int argc, char * argv[]){
  struct sigation sa;
  char * port = argv[1];
  sigation(SIGTERM, NULL, &sa);
  sa.sa_handler = handler;
  sigation(SIGTERM, &sa, NULL);
  int s = create_server_socket(port);
  while(true){
    int sd = accept_connection(s);
    if(quit) break;
    dial(sd);
    close(sd);
    if(quit) break;
  }
  close(s);
  return 0;
}
