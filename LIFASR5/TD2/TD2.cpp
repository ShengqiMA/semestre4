//exo 2 2.3.
int main(){
  int r1 = fork();
  if(r1 == 0){
    return 0;
  }
  int r2 = fork();
  if(r2 == 0){
    int r3 = fork();
    if(r3 == 0){
      return 0;
    }
    int r4 = fork();
    if(r4 == 0){
      return 0;
    }
    waitpid(r4,NULL,0);
    waitpid(r3,NULL,0);
  }
  waitpid(r1,NULL,0);
  waitpid(r2,NULL,0);
  return 0;
}

int main(){
  int pid[4];
  for(int i = 0;i < 4;i++){
    if((pid[i] = fork()) == 0){
      return 0;
    }
    for(int i = 0;i < 4;i++){
      waitpid(pid[i],NULL,0);
    }
  }
}
