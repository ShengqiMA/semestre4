#include <signal.h>
#include <iostream>
using namespace std;

// un compteur global
int cpt = 0;
// gestionnaire de signal
void fct(int s){
  cpt++;
  cout << "M’enfin ! (" << cpt << ")"<< endl;
  if (cpt == 10) {
    cout << "Aie !!!" << endl;
    exit(EXIT_SUCCESS);
  }
}



int main(void) {
  // installation du gestionnaire pour SIGINT (Ctrl+c)
  struct sigaction s;
  //if( sigaction(SIGINT, NULL, &s) == -1 ) exit(EXIT_FAILURE);
  s.sa_handler = fct;
  if( sigaction(SIGINT, &s, NULL) == -1 ) exit(EXIT_FAILURE);
  while(1) {
    cout << "RRRRR..." << endl;
    sleep(1);
  }
  return EXIT_SUCCESS;
}
